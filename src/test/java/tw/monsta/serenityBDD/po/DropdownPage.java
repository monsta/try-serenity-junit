package tw.monsta.serenityBDD.po;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Optional;

/**
 * DropdownPage description
 *
 * @author johnson
 * @since 2015-08-18
 */
@DefaultUrl("http://the-internet.herokuapp.com/dropdown")
public class DropdownPage extends PageObject {

    //用serenity的WebElementFacade，提供更多方便的API
    @FindBy(id = "dropdown")
    WebElementFacade dropdown;

    @FindBy(css = "#dropdown > option")
    List<WebElement> dropdownOptions;

    public Optional<List<WebElement>> getAllDropdownElement() {
        return Optional.of(dropdownOptions);
    }

    public boolean dropdownElementIsVisible() {
        return dropdown.isCurrentlyVisible();
    }

    public void checkDropdownIsVisible() {
        dropdown.shouldBeVisible();
    }

    public WebElementFacade dropdownElement() {
        return element(dropdown);
    }
}
