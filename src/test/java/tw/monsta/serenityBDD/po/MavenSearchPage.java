package tw.monsta.serenityBDD.po;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Map;

import static net.thucydides.core.pages.components.HtmlTable.rowsFrom;

/**
 * MavenSearchPage description
 *
 * @author johnson
 * @since 2015-08-19
 */
@DefaultUrl("http://search.maven.org/")
public class MavenSearchPage extends PageObject {

    private WebElement query;
    private WebElement queryButton;
    private WebElement resultTable;

    public void enterSearchTerm(String term) {
        element(query).sendKeys(term);
    }

    public void startSearch() {
        element(queryButton).click();
    }

    public List<Map<Object, String>> getSearchResults() {
        return rowsFrom(resultTable);
    }
}
