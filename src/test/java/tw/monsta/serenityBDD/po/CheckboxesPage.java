package tw.monsta.serenityBDD.po;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

/**
 * CheckboxesPage description
 *
 * @author johnson
 * @since 2015-08-12
 */
@DefaultUrl("http://the-internet.herokuapp.com/checkboxes")
public class CheckboxesPage extends PageObject {
}