package tw.monsta.serenityBDD.po;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.titleContains;

/**
 * GooglePage description
 *
 * @author johnson
 * @since 2015-08-13
 */
@DefaultUrl("http://www.google.com.tw")
public class GooglePage extends PageObject {

    @FindBy(name="q")
    WebElement search;

    public void searchFor(String term) {
        search.sendKeys(term, Keys.ENTER);
        waitFor(titleContains("Google 搜尋"));
    }
}
