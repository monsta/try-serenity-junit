package tw.monsta.serenityBDD.features.checkboxPage;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import tw.monsta.serenityBDD.po.CheckboxesPage;
import tw.monsta.serenityBDD.steps.CheckboxesPageSteps;

/**
 * 嘗試搭配Page Object測試Serenity
 *
 * @author johnson
 * @since 2015-08-12
 */
@RunWith(SerenityRunner.class)
public class WhenBrowseCheckboxesPage {

    CheckboxesPage checkboxesPage;

    /**
     * 雖然沒有直接用到這個field，但對Serenity來說需要此managed resource
     * 才有辦法instantiate Page Object
     */
    @Managed
    WebDriver webDriver;

    @Steps
    private CheckboxesPageSteps checkboxesPageSteps;

    @Test
    public void pageShouldContainsCheckboxes() {
        //GIVEN
        checkboxesPageSteps.is_ready_to_browse(checkboxesPage);
        /*還是用steps去包page object，因為目前已知Serenity只會在step才儲存畫面
         * 也許嘗試用selenium grid會發現根本無法儲存畫面??
         **/

        //THEN
        checkboxesPageSteps.has_checkbox_elements();
    }

    @Test
    @Pending
    public void pageShouldContainsTitle() {

    }
}
