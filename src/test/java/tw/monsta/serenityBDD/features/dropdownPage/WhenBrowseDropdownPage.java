package tw.monsta.serenityBDD.features.dropdownPage;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import tw.monsta.serenityBDD.po.DropdownPage;
import tw.monsta.serenityBDD.steps.DropdownPageSteps;

/**
 * 直接使用web driver做測試
 *
 * @author johnson
 * @since 2015-08-12
 */
@RunWith(SerenityRunner.class)
public class WhenBrowseDropdownPage {

    DropdownPage page;
    //Serenity會自動instantiate這個field instance
    @Steps
    DropdownPageSteps pageSteps;

    //selenium預設會用firefox，如果沒有指定driver=""的話
    //測試同一個頁面，用unique browser session比較快
    @Managed(uniqueSession = true)
    private WebDriver webDriver;

    @Test
    public void shouldSeeDropdownListWithMoreThanOneOptions() {
        //GIVEN & WHEN
        pageSteps.open_dropdown_page(page);

        //THEN
        pageSteps.page_should_have_a_dropdown_element_with_options(3);
    }

    @Test
    public void dropdownListShouldBeVisible() {
        pageSteps.open_dropdown_page(page);
        pageSteps.should_see_dropdown_list();
    }
}
