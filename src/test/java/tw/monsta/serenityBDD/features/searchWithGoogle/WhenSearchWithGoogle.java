package tw.monsta.serenityBDD.features.searchWithGoogle;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.Concurrent;
import net.thucydides.junit.annotations.TestData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import tw.monsta.serenityBDD.po.GooglePage;
import tw.monsta.serenityBDD.steps.GoogleSteps;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 使用Serenity的test data方式做Data Driven Test
 * 並加上concurrent做同步測試執行
 *
 * @author johnson
 * @since 2015-08-13
 */
@RunWith(SerenityParameterizedRunner.class)
@Concurrent(threads = "2x")
public class WhenSearchWithGoogle {

    @Steps
    private GoogleSteps googleSteps;

    private GooglePage googlePage;

    @Managed
    WebDriver webDriver;

    @TestData
    public static Collection<Object[]> testData() {
        return Arrays.asList(new Object[][]{
            {"cats"},
            {"dogs"},
            {"rabbits"},
            {"lions"},
            {"elephants"},
            {"birds"}
        });
    }

    private final String term;

    public WhenSearchWithGoogle(String term) {
        this.term = term;
    }

    @Test
    public void shouldFindSomething() {
        //ASSUMPTION
        googleSteps.is_ready(googlePage);

        //WHEN
        googleSteps.searchFor(term);

        //THEN
        assertThat(googlePage.getTitle()).isEqualTo(term + " - Google 搜尋");
    }
}
