package tw.monsta.serenityBDD.features.usingFluentMatcher;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.Pages;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import tw.monsta.serenityBDD.steps.DeveloperSteps;

import static net.thucydides.core.matchers.BeanMatchers.each;
import static net.thucydides.core.matchers.BeanMatchers.the_count;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;

/**
 * WhenSearchForArtifacts description
 *
 * @author johnson
 * @since 2015-08-19
 */
@RunWith(SerenityRunner.class)
public class WhenSearchForArtifacts {

    @Managed
    WebDriver webDriver;

    @ManagedPages
    public Pages pages;

    @Steps
    public DeveloperSteps developer;

    @Test
    public void shouldFindTheRightNumberOfArtifacts() {
        developer.opensTheSearchPage();
        developer.searchFor("serenity-bdd");
        developer.shouldSeeArtifactsWhere(
            each("ArtifactId").isDifferent()
            , the_count(is(greaterThanOrEqualTo(16)))
        );
    }
}
