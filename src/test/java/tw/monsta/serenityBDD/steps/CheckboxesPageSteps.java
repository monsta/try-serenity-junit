package tw.monsta.serenityBDD.steps;

import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import tw.monsta.serenityBDD.po.CheckboxesPage;

import static org.assertj.core.api.StrictAssertions.assertThat;

/**
 * CheckboxesPageSteps description
 *
 * @author johnson
 * @since 2015-08-12
 */
public class CheckboxesPageSteps {

    CheckboxesPage checkboxesPage;

    /**
     * 打開頁面
     * @param checkboxesPage
     */
    @Step("Given checkboxes page is open")
    public void is_ready_to_browse(CheckboxesPage checkboxesPage) {
        this.checkboxesPage = checkboxesPage;
        this.checkboxesPage.open();
    }

    @Step("Then page should contains checkbox elements")
    public void has_checkbox_elements() {
        assertThat(this.checkboxesPage.containsElements(
                By.cssSelector("input[type=\"checkbox\"]"))
        ).isTrue();
    }
}
