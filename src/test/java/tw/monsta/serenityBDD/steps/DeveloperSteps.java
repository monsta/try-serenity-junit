package tw.monsta.serenityBDD.steps;

import net.thucydides.core.annotations.Screenshots;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.matchers.BeanMatcher;
import net.thucydides.core.matchers.BeanMatcherAsserts;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;
import tw.monsta.serenityBDD.po.MavenSearchPage;

import java.util.List;
import java.util.Map;

/**
 * DeveloperSteps description
 *
 * @author johnson
 * @since 2015-08-19
 */
public class DeveloperSteps extends ScenarioSteps {

    public DeveloperSteps(Pages pages) {
        super(pages);
    }

    @Step("當使用者開啟maven search page")
    public void opensTheSearchPage() {
        onSearchPage().open();
    }

    private MavenSearchPage onSearchPage() {
        return getPages().get(MavenSearchPage.class);
    }

    @Step("查詢{0}")
    public void searchFor(String term) {
        onSearchPage().enterSearchTerm(term);
        onSearchPage().startSearch();
    }

    @Step("應該符合這些條件{0}")
    @Screenshots(afterEachStep = true)
    public void shouldSeeArtifactsWhere(BeanMatcher... matchers) {
        List<Map<Object, String>> searchResults = onSearchPage().getSearchResults();
        BeanMatcherAsserts.shouldMatch(searchResults, matchers);
//        Serenity.takeScreenshot();
    }
}
