package tw.monsta.serenityBDD.steps;

import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tw.monsta.serenityBDD.po.DropdownPage;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * DropdownPageSteps description
 *
 * @author johnson
 * @since 2015-08-12
 */
public class DropdownPageSteps {

    private DropdownPage dropdownPage;

    @Step("When user opens dropdown page")
    public void open_dropdown_page(DropdownPage dropdownPage) {
        this.dropdownPage = dropdownPage;
        this.dropdownPage.open();
    }

    @Step("Should see a dropdown list with {0} option(s)")
    public void page_should_have_a_dropdown_element_with_options(int amountOfOptions) {
        assertThat(dropdownPage.getAllDropdownElement().get()).hasSize(amountOfOptions);
    }

    @Step("Dropdown element should be visible")
    public void should_see_dropdown_list() {
        //check if element is visible
//        assertThat(dropdownPage.dropdownElementIsVisible()).isTrue();

        //or using page object's assertion API
//        dropdownPage.checkDropdownIsVisible();

        //也可以用非常fluent style寫
        dropdownPage.dropdownElement().shouldBeVisible();
    }
}
