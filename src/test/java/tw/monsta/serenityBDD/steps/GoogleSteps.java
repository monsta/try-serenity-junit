package tw.monsta.serenityBDD.steps;

import net.thucydides.core.annotations.Step;
import org.hamcrest.Matchers;
import tw.monsta.serenityBDD.po.GooglePage;

import static org.junit.Assume.assumeThat;

/**
 * 在searchFor時，無法記錄輸入search key的畫面
 *
 * @author johnson
 * @since 2015-08-13
 */
public class GoogleSteps {

    private GooglePage googlePage;

    @Step
    public void is_ready(GooglePage googlePage) {
        this.googlePage = googlePage;
        this.googlePage.open();
        assumeThat(this.googlePage.getTitle(), Matchers.equalTo("Google"));
    }

    @Step
    public void searchFor(String term) {
        this.googlePage.searchFor(term);
    }
}
